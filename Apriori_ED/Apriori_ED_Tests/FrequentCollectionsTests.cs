using Apriori_ED.Models;
using FakeItEasy;
using NUnit.Framework;
using System.Collections.Generic;
using Apriori_ED.Logic;

namespace Apriori_ED_Tests
{
    public class FrequentCollectionsTests
    {
        private FrequentCollectionsCalculations _sut;

        [SetUp]
        public void Setup()
        {
            _sut = new FrequentCollectionsCalculations();
            Settings.SupportThreshold = 1;
        }

        [Test]
        public void GetFirstLevelItems_GivenInput_ReturnListOfItems()
        {
            // Arrange
            var expected = new List<int> { 4, 5, 6, 7, 8, 9 };

            // Act
            var actual = _sut.GetFirstLevelUniqueElements(AprioriTestCollections.MainInput);

            // Assert
            CollectionAssert.AreEqual(expected, actual);
        }

        [Test]
        public void GetCollectionsForLevel_GivenFirstLevel_ReturnsCorrectCollectionWithSupport()
        {
            // Arrange
            var expected = AprioriTestCollections.FirstLevelCollectionsBeforeThreshold;

            // Act
            var actual = _sut.GetCollectionsForFirstLevel(AprioriTestCollections.MainInput);

            // Assert
            CollectionAssert.AreEquivalent(expected, actual);
        }

        [Test]
        public void GetCollectionsForNextLevel_GivenCollection_ReturnCollectionWithSupport()
        {
            // Arrange
            var inputCollection = AprioriTestCollections.MainInput;
            var firstLevelCollection = AprioriTestCollections.FirstLevelCollections;
            var nextLevelCollection = AprioriTestCollections.SecondLevelCollectionsBeforeThreshold;

            // Act
            var actual = _sut.GetCollectionForNextLevel(inputCollection, firstLevelCollection);

            // Assert
            CollectionAssert.AreEquivalent(nextLevelCollection, actual);
        }

        private static object[] _countSupportCollection =
        {
            new object[]
            {
                AprioriTestCollections.MainInput,
                AprioriTestCollections.FirstLevelCollectionsWithoutSupport,
                AprioriTestCollections.FirstLevelCollections
            },
            new object[]
            {
                AprioriTestCollections.MainInput,
                AprioriTestCollections.SecondLevelCollectionsWithoutSupport,
                AprioriTestCollections.SecondLevelCollections
            },
        };

        [TestCaseSource(nameof(_countSupportCollection))]
        public void CountSupport_GivenInputAndCurrentCollection_CountSupportForCurrent(List<AprioriCollection> input, List<AprioriCollection> current, List<AprioriCollection> expectedCurrent)
        {
            // Arrange

            // Act
            _sut.CountSupport(input, current);

            // Assert
            CollectionAssert.AreEqual(expectedCurrent, current);
        }

        private static object[] _applyThresholdCollection =
        {
            new object[]
            {
                new List<AprioriCollection>
                {
                    new AprioriCollection(A.Dummy<List<int>>(), 2),
                    new AprioriCollection(A.Dummy<List<int>>(), 2),
                    new AprioriCollection(A.Dummy<List<int>>(), 1),
                    new AprioriCollection(A.Dummy<List<int>>(), 1),
                },
                1,
                new List<AprioriCollection>
                {
                    new AprioriCollection(A.Dummy<List<int>>(), 2),
                    new AprioriCollection(A.Dummy<List<int>>(), 2),
                },
            }
        };

        [TestCaseSource(nameof(_applyThresholdCollection))]
        public void ApplyThreshold_GivenCollections_RemoveUnderThreshold(List<AprioriCollection> collection, int threshold, List<AprioriCollection> expected)
        {
            // Arrange
            Settings.SupportThreshold = threshold;

            // Act
            var actual = _sut.ApplyThreshold(collection);

            // Assert
            CollectionAssert.AreEquivalent(expected, actual);
        }



        [Test]
        public void GetFrequentCollections_GivenCorrectInput_ReturnFrequentCollections()
        {
            // Arrange
            var expected = new List<AprioriCollection>();
            expected.AddRange(AprioriTestCollections.FirstLevelCollections);
            expected.AddRange(AprioriTestCollections.SecondLevelCollections);
            expected.AddRange(AprioriTestCollections.ThirdLevelCollections);

            // Act
            var actual = _sut.GetFrequentCollections(AprioriTestCollections.MainInput);

            // Assert
            CollectionAssert.AreEquivalent(expected, actual);
        }
    }
}