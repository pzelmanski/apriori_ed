﻿using System.Collections.Generic;
using Apriori_ED.Models;

namespace Apriori_ED_Tests
{
    class AprioriTestCollections
    {
        public static readonly List<AprioriCollection> MainInput = new List<AprioriCollection>
        {
            new AprioriCollection(new List<int> {5, 6, 8, 9}),
            new AprioriCollection(new List<int> {5, 6, 7, 9}),
            new AprioriCollection(new List<int> {7, 8, 9}),
            new AprioriCollection(new List<int> {4, 6, 9}),
            new AprioriCollection(new List<int> {5, 7, 8, 9}),
        };

        public static readonly List<AprioriCollection> FirstLevelCollectionsWithoutSupport = new List<AprioriCollection>()
        {
            new AprioriCollection(new List<int> {5}),
            new AprioriCollection(new List<int> {6}),
            new AprioriCollection(new List<int> {7}),
            new AprioriCollection(new List<int> {8}),
            new AprioriCollection(new List<int> {9}),
        };

        public static readonly List<AprioriCollection> FirstLevelCollections = new List<AprioriCollection>()
        {
            new AprioriCollection(new List<int> {5}, 3),
            new AprioriCollection(new List<int> {6}, 3),
            new AprioriCollection(new List<int> {7}, 3),
            new AprioriCollection(new List<int> {8}, 3),
            new AprioriCollection(new List<int> {9}, 5),
        };

        public static readonly List<AprioriCollection> FirstLevelCollectionsBeforeThreshold = new List<AprioriCollection>()
        {
            new AprioriCollection(new List<int> {4}, 1),
            new AprioriCollection(new List<int> {5}, 3),
            new AprioriCollection(new List<int> {6}, 3),
            new AprioriCollection(new List<int> {7}, 3),
            new AprioriCollection(new List<int> {8}, 3),
            new AprioriCollection(new List<int> {9}, 5),
        };

        public static readonly List<AprioriCollection> SecondLevelCollectionsWithoutSupport = new List<AprioriCollection>()
        {
            new AprioriCollection(new List<int> {5, 6}),
            new AprioriCollection(new List<int> {5, 7}),
            new AprioriCollection(new List<int> {5, 8}),
            new AprioriCollection(new List<int> {5, 9}),
            new AprioriCollection(new List<int> {6, 9}),
            new AprioriCollection(new List<int> {7, 8}),
            new AprioriCollection(new List<int> {7, 9}),
            new AprioriCollection(new List<int> {8, 9}),
        };

        public static readonly List<AprioriCollection> SecondLevelCollections = new List<AprioriCollection>()
        {
            new AprioriCollection(new List<int> {5, 6}, 2),
            new AprioriCollection(new List<int> {5, 7}, 2),
            new AprioriCollection(new List<int> {5, 8}, 2),
            new AprioriCollection(new List<int> {5, 9}, 3),
            new AprioriCollection(new List<int> {6, 9}, 3),
            new AprioriCollection(new List<int> {7, 8}, 2),
            new AprioriCollection(new List<int> {7, 9}, 3),
            new AprioriCollection(new List<int> {8, 9}, 3),
        };

        public static readonly List<AprioriCollection> SecondLevelCollectionsBeforeThreshold = new List<AprioriCollection>()
        {
            new AprioriCollection(new List<int> {5, 6}, 2),
            new AprioriCollection(new List<int> {5, 7}, 2),
            new AprioriCollection(new List<int> {5, 8}, 2),
            new AprioriCollection(new List<int> {5, 9}, 3),
            new AprioriCollection(new List<int> {6, 7}, 1),
            new AprioriCollection(new List<int> {6, 8}, 1),
            new AprioriCollection(new List<int> {6, 9}, 3),
            new AprioriCollection(new List<int> {7, 8}, 2),
            new AprioriCollection(new List<int> {7, 9}, 3),
            new AprioriCollection(new List<int> {8, 9}, 3),
        };

        public static readonly List<AprioriCollection> ThirdLevelCollectionsWithoutSupport = new List<AprioriCollection>()
        {
            new AprioriCollection(new List<int> {5, 8, 9}),
        };

        public static readonly List<AprioriCollection> ThirdLevelCollections = new List<AprioriCollection>()
        {
            new AprioriCollection(new List<int> {5, 6, 9}, 2),
            new AprioriCollection(new List<int> {5, 7, 9}, 2),
            new AprioriCollection(new List<int> {5, 8, 9}, 2),
            new AprioriCollection(new List<int> {7, 8, 9}, 2),
        };

        public static readonly List<AprioriCollection> ThirdLevelCollectionsBeforeThreshold = new List<AprioriCollection>()
        {
            new AprioriCollection(new List<int> {5, 6, 9}, 2),
            new AprioriCollection(new List<int> {5, 7, 8}, 1),
            new AprioriCollection(new List<int> {5, 7, 9}, 2),
            new AprioriCollection(new List<int> {5, 8, 9}, 2),
            new AprioriCollection(new List<int> {7, 8, 9}, 2),
        };
    }
}
