﻿using Apriori_ED.Logic;
using Apriori_ED.Models;
using NUnit.Framework;
using System.Collections.Generic;

namespace Apriori_ED_Tests
{
    [TestFixture]
    class RulesGeneratorTests
    {
        private RulesGenerator _sut;
        [SetUp]
        public void SetUp()
        {
            _sut = new RulesGenerator();
        }

        [Test]
        public void GenerateRules_GivenSingleItem_NoRulesAreGenerated()
        {
            // Arrange
            var collection = new AprioriCollection(new List<int> { 3 }, 5);

            // Act
            var actual = _sut.GenerateRulesFromCollection(collection);

            // Assert
            CollectionAssert.IsEmpty(actual);
        }

        [Test]
        public void GenerateRules_Given2elementCollection_RulesAreGenerated()
        {
            // Arrange
            var collection = new AprioriCollection(new List<int> { 2, 3 }, 5);
            var expected = new List<Rule>
            {
                new Rule(new List<int>{2}, new List<int>{3}),
                new Rule(new List<int>{3}, new List<int>{2})
            };

            // Act
            var actual = _sut.GenerateRulesFromCollection(collection);

            // Assert
            CollectionAssert.AreEquivalent(expected, actual);
        }

        [Test]
        public void GenerateRules_Given3OrMoreElementsCollection_RulesAreGenerated()
        {
            // Arrange
            var collection = new AprioriCollection(new List<int> { 1, 2, 3 }, 5);
            var expected = new List<Rule>
            {
                new Rule(new List<int>{1}, new List<int>{2, 3}),
                new Rule(new List<int>{2}, new List<int>{1, 3}),
                new Rule(new List<int>{3}, new List<int>{1, 2}),
                new Rule(new List<int>{1, 2}, new List<int>{3}),
                new Rule(new List<int>{1, 3}, new List<int>{2}),
                new Rule(new List<int>{2, 3}, new List<int>{1}),
            };

            // Act
            var actual = _sut.GenerateRulesFromCollection(collection);

            // Assert
            CollectionAssert.AreEquivalent(expected, actual);
        }

        [Test]
        public void GetChildRules_GivenParent_ReturnsRules()
        {
            var rule = new Rule(new List<int> { 2, 3 }, new List<int>());
            var expected = new List<Rule>
            {
                new Rule(new List<int>{2}, new List<int>{3}),
                new Rule(new List<int>{3}, new List<int>{2})
            };

            // Act
            var actual = _sut.GetChildRulesFromRule(rule, new List<Rule>());

            // Assert
            CollectionAssert.AreEquivalent(expected, actual);
        }

        [Test]
        public void GetChildRules_Given3ElementParent_ReturnsRules()
        {
            // Arrange
            var rule = new Rule(new List<int> { 1, 2, 3 }, new List<int>());
            var expected = new List<Rule>
            {
                new Rule(new List<int>{1}, new List<int>{2, 3}),
                new Rule(new List<int>{2}, new List<int>{1, 3}),
                new Rule(new List<int>{3}, new List<int>{1, 2}),
                new Rule(new List<int>{1, 2}, new List<int>{3}),
                new Rule(new List<int>{1, 3}, new List<int>{2}),
                new Rule(new List<int>{2, 3}, new List<int>{1}),
            };

            // Act
            var actual = _sut.GetChildRulesFromRule(rule, new List<Rule>());

            // Assert
            CollectionAssert.AreEquivalent(expected, actual);
        }
        [Test]
        public void GetChildRules_Given4ElementParent_ReturnsRules()
        {
            // Arrange
            var rule = new Rule(new List<int> { 1, 2, 3, 4 }, new List<int>());
            var expected = new List<Rule>
            {
                new Rule(new List<int> {1,}, new List<int> {2, 3, 4}),
                new Rule(new List<int> {2,}, new List<int> {1, 3, 4}),
                new Rule(new List<int> {3,}, new List<int> {1, 2, 4}),
                new Rule(new List<int> {4,}, new List<int> {1, 2, 3}),
                new Rule(new List<int> {1, 2,}, new List<int> {3, 4}),
                new Rule(new List<int> {1, 3,}, new List<int> {2, 4}),
                new Rule(new List<int> {1, 4,}, new List<int> {2, 3}),
                new Rule(new List<int> {2, 3,}, new List<int> {1, 4}),
                new Rule(new List<int> {2, 4,}, new List<int> {1, 3}),
                new Rule(new List<int> {3, 4,}, new List<int> {1, 2}),
                new Rule(new List<int> {1, 2, 3,}, new List<int> {4}),
                new Rule(new List<int> {1, 2, 4,}, new List<int> {3}),
                new Rule(new List<int> {1, 3, 4,}, new List<int> {2}),
                new Rule(new List<int> {2, 3, 4,}, new List<int> {1}),
            };

            // Act
            var actual = _sut.GetChildRulesFromRule(rule, new List<Rule>());

            // Assert
            CollectionAssert.AreEquivalent(expected, actual);
        }
    }
}
