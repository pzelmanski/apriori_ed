﻿using System.Collections.Generic;
using Apriori_ED.Models;
using NUnit.Framework;

namespace Apriori_ED_Tests
{
    [TestFixture]
    class AprioriCollectionTests
    {

        [SetUp]
        public void SetUp()
        {
            Settings.SupportThreshold = 1;
        }

        private static object[] _hasCollectionColections = {
            new object[] {new List<int> {1, 2, 3, 4}, new List<int> {1, 2}, true},
            new object[] {new List<int> {1, 2, 3, 4}, new List<int> {1, 4}, true},
            new object[] {new List<int> {1, 2, 3, 4}, new List<int> {1, 5}, false},
        };

        private static object[] _canCombineCollection = {
            new object[] {new List<int> {1}, new List<int> {2}, true},
            new object[] {new List<int> {1, 2}, new List<int> {2, 3}, true},
            new object[] {new List<int> {1, 2}, new List<int> {3, 4}, false},
        };

        private static object[] _combineCollection = {
            new object[] {new List<int> {1}, new List<int> {2}, new List<int> {1, 2}},
            new object[] {new List<int> {1, 2}, new List<int> {2, 3}, new List<int> {1, 2, 3}},
        };

        [Test]
        [TestCaseSource(nameof(_hasCollectionColections))]
        public void HasCollection_GivenCollection_ReturnsCorrectly(List<int> collection, List<int> checkedCombination, bool expected)
        {
            // Arrange
            var sut = new AprioriCollection(collection);

            // Act
            var actual = sut.ContainsCombination(checkedCombination);

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [TestCaseSource(nameof(_canCombineCollection))]
        public void CanCombine_GivenCollection_ReturnsCorrectly(List<int> collection, List<int> toCombine,
            bool expected)
        {
            // Arrange
            var sut = new AprioriCollection(collection);

            // Act
            var actual = sut.CanCombine(toCombine);

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [TestCaseSource(nameof(_combineCollection))]
        public void Combine_GivenTwoCorrectCollections_Combines(List<int> firstCollection, List<int> secondCollection,
            List<int> expected)
        {
            // Arrange
            var sut = new AprioriCollection(firstCollection);

            // Act
            var actual = sut.Combine(secondCollection);

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [Test]
        [TestCase(1, 1, true)]
        [TestCase(1, 2, false)]
        [TestCase(2, 1, true)]
        public void ShouldRemoveCollection_GivenSupport_ReturnValue(int threshold, int support, bool expected)
        {
            // Arrange
            var sut = new AprioriCollection(null, support);
            Settings.SupportThreshold = threshold;
            
            // Act
            var actual = sut.ShouldRemoveCollection();
            
            // Assert
            Assert.AreEqual(expected, actual);
        }
    }
}
