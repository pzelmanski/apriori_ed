﻿using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;

namespace Apriori_ED.Models
{
    public class AprioriCollection
    {
        public List<int> Items { get; }

        public int Support;

        public AprioriCollection() { }

        public AprioriCollection(List<int> items, int support = 0)
        {
            Items = items;
            Support = support;
        }

        public bool ContainsCombination(List<int> combination)
        {
            foreach (var item in combination)
            {
                if (!Items.Contains(item))
                {
                    return false;
                }
            }

            return true;
        }

        public bool CanCombine(List<int> toCombine)
        {
            if (Items.Last() != toCombine.First() && Items.Count > 1)
            {
                return false;
            }
            var combined = new List<int>();
            combined.AddRange(Items);
            combined.AddRange(toCombine);
            combined.Sort();
            var unique = combined.Distinct();

            return unique.Count() == toCombine.Count + 1;
        }

        public List<int> Combine(List<int> toCombine)
        {
            var combined = new List<int>();
            combined.AddRange(Items);
            combined.AddRange(toCombine);
            combined.Sort();
            combined = combined.Distinct().ToList();

            return combined;
        }


        public bool ShouldRemoveCollection()
        {
            return Settings.SupportThreshold >= this.Support;
        }

        public override bool Equals(object obj)
        {
            var compared = obj as AprioriCollection ?? new AprioriCollection();

            return compared.Items.SequenceEqual(Items) && compared.Support == Support;
        }

        public override string ToString()
        {
            var returned = "Collection: ";
            foreach (var item in Items)
            {
                returned += item + ", ";
            }

            returned += "support: " + Support;
            return returned;
        }
    }
}
