﻿using System.Collections.Generic;
using System.Linq;

namespace Apriori_ED.Models
{
    public class Rule
    {
        public Rule(List<int> condition, List<int> value)
        {
            Condition = condition;
            Value = value;
        }

        public List<int> Condition;

        public List<int> Value;

        public override bool Equals(object obj)
        {
            var compared = obj as Rule ?? new Rule(new List<int>(), new List<int>());

            return compared.Condition.SequenceEqual(Condition) && compared.Value.SequenceEqual(Value);
        }

        public override string ToString()
        {
            var returned = "";

            foreach (var c in Condition)
            {
                returned += $" {c} ";
            }

            returned += " => ";

            foreach (var v in Value)
            {
                returned += $" {v} ";
            }

            return returned;
        }
    }
}
