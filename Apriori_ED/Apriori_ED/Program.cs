﻿using System;
using Apriori_ED.Models;
using System.Collections.Generic;
using Apriori_ED.Logic;

namespace Apriori_ED
{
    class Program
    {
        static void Main(string[] args)
        {
            Settings.SupportThreshold = 2;
            /*
                {M, O, N, K, E, Y }
                {D, O, N, K, E, Y }
                {M, A, K, E}
                {M, U, C, K, Y }
                {C, O, O, K, I, E}
             */
            List<AprioriCollection> monkey = new List<AprioriCollection>
            {
                new AprioriCollection(new List<int> {1, 2, 3, 4, 5, 6}),
                new AprioriCollection(new List<int> {7, 2, 3, 4, 5, 6}),
                new AprioriCollection(new List<int> {1, 8, 4, 5}),
                new AprioriCollection(new List<int> {1, 9, 10, 4, 6}),
                new AprioriCollection(new List<int> {10, 2, 2, 4, 11, 5}),
            };

            var result = new FrequentCollectionsCalculations().GetFrequentCollections(monkey);

            var rulesGenerator = new RulesGenerator();

            Console.WriteLine("Frequent collections: ");
            Console.WriteLine("========================");
            foreach (var frequentCollection in result)
            {
                Console.Write(frequentCollection.ToString());

                Console.Write(" ||| Association rules: ");

                var rules = rulesGenerator.GenerateRulesFromCollection(frequentCollection);

                if (rules.Count == 0)
                {
                    Console.Write(" { } ");
                }

                foreach (var rule in rules)
                {
                    Console.Write($"{{ {rule} }}");
                }

                Console.WriteLine();
            }

            Console.ReadKey();
        }
    }
}
