﻿using Apriori_ED.Models;
using System.Collections.Generic;
using System.Linq;

namespace Apriori_ED.Logic
{
    public class RulesGenerator
    {
        public List<Rule> GenerateRulesFromCollection(AprioriCollection collection)
        {
            var root = new Rule(collection.Items, new List<int>());

            var returned = GetChildRulesFromRule(root, new List<Rule>());

            return returned;
        }

        public List<Rule> GetChildRulesFromRule(Rule parent, List<Rule> allRules)
        {
            if (parent.Condition.Count == 1)
                return new List<Rule>();

            var childRules = new List<Rule>();

            for (int i = 0; i < parent.Condition.Count; i++)
            {
                var childConditions = new List<int>();
                childConditions.AddRange(parent.Condition);
                childConditions.RemoveAt(i);

                var childValues = new List<int>();
                childValues.AddRange(parent.Value);
                childValues.Add(parent.Condition[i]);

                if (!allRules.Exists(x => x.Condition.All(childConditions.Contains)
                                          && x.Condition.Count == childConditions.Count))
                {
                    childRules.Add(new Rule(childConditions, childValues));
                }
            }

            var returned = new List<Rule>();
            returned.AddRange(childRules);
            foreach (var rule in childRules)
            {
                returned.AddRange(GetChildRulesFromRule(rule, allRules));
            }

            allRules.AddRange(returned);

            return returned;
        }

    }
}
