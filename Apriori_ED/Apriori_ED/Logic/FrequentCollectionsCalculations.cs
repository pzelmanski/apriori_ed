﻿using System.Collections.Generic;
using System.Linq;
using Apriori_ED.Models;

namespace Apriori_ED.Logic
{
    public class FrequentCollectionsCalculations
    {
        public List<AprioriCollection> GetFrequentCollections(List<AprioriCollection> input)
        {
            List<AprioriCollection> frequentCollections = new List<AprioriCollection>();

            var first = GetCollectionsForFirstLevel(input);
            first = ApplyThreshold(first);
            frequentCollections.AddRange(first);

            var previousCollection = first;
            while (previousCollection.Count != 0)
            {
                previousCollection = GetCollectionForNextLevel(input, previousCollection);
                previousCollection = ApplyThreshold(previousCollection);
                frequentCollections.AddRange(previousCollection);
            }

            return frequentCollections;
        }

        public List<AprioriCollection> GetCollectionForNextLevel(List<AprioriCollection> inputCollection, List<AprioriCollection> previousLevel)
        {
            var nextLevelCollection = new List<AprioriCollection>();
            for (int i = 0; i < previousLevel.Count; i++)
            {
                for (int j = i; j < previousLevel.Count; j++)
                {
                    var canCombine = previousLevel[i].CanCombine(previousLevel[j].Items);
                    if (canCombine)
                    {
                        var combination = previousLevel[i].Combine(previousLevel[j].Items);

                        if (!CombinationAlreadyExists(nextLevelCollection, combination))
                        {
                            nextLevelCollection.Add(new AprioriCollection(combination));
                        }
                    }
                }
            }

            CountSupport(inputCollection, nextLevelCollection);

            return nextLevelCollection;
        }

        private bool CombinationAlreadyExists(List<AprioriCollection> collection, List<int> combination)
        {
            return collection.Any(x => x.ContainsCombination(combination));
        }

        public List<AprioriCollection> GetCollectionsForFirstLevel(List<AprioriCollection> input)
        {
            var elements = GetFirstLevelUniqueElements(input);
            var nextLevelCollection = new List<AprioriCollection>();
            foreach (var element in elements)
            {
                nextLevelCollection.Add(new AprioriCollection(new List<int> { element }));
            }

            this.CountSupport(input, nextLevelCollection);

            return nextLevelCollection;
        }

        public void CountSupport(List<AprioriCollection> input, List<AprioriCollection> currentCollection)
        {
            foreach (var c in currentCollection)
            {
                foreach (var inputItem in input)
                {
                    if (inputItem.ContainsCombination(c.Items))
                    {
                        c.Support++;
                    }
                }
            }
        }

        public List<AprioriCollection> ApplyThreshold(List<AprioriCollection> inputs)
        {
            return inputs.Where(x => !x.ShouldRemoveCollection()).ToList();
        }

        public List<int> GetFirstLevelUniqueElements(List<AprioriCollection> inputs)
        {
            var result = new List<int>();
            foreach (var input in inputs)
            {
                result.AddRange(input.Items);
            }

            result.Sort();
            return result.Distinct().ToList();
        }
    }
}
